<footer id="footer" class="footer-container">

	<div id="footer-social">
		<div class="widget social">
			<?php
			$defaults = array(
				'theme_location'  => 'footer-social',
				'container'       => 'div',
				'container_class' => 'social footer-social',
				'container_id'    => 'social_section',
				'menu_class'      => 'menu',
				'menu_id'         => 'menu-social',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);
			wp_nav_menu( $defaults );
			?>
		</div>
	</div>

	<div id="footer-widgets">
		<div class="widget">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-col-1') ) : ?>
			<?php endif; ?>
		</div>
	
		<div class="widget">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-col-2') ) : ?>
			<?php endif; ?>
		</div>
	
		<div class="widget">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-col-3') ) : ?>
			<?php endif; ?>
		</div>
	
		<div class="widget">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-col-4') ) : ?>
			<?php endif; ?>
		</div>
	</div>
		
	<div id="footer-copyright" class="clearfix hundredpercent">
		<div class="copyright col-lg-6 col-md-6 col-sm-6 col-xs-12">
			&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.
		</div>
		<div class="content col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="wrapper clearfix">
				<div class="links">
					<?php
					$defaults = array(
						'theme_location'  => 'footer-links',
						'container'       => 'div',
						'container_class' => 'links footer-links',
						'container_id'    => 'links_section',
						'menu_class'      => 'menu',
						'menu_id'         => 'menu-links',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);
					wp_nav_menu( $defaults );
					?>
				</div>
				<div class="cookieLine">
					By using this website, you agree to our <a href="<?php echo home_url(); ?>/cookie-policy/" class="footer-link">Cookie Policy</a>.
				</div>
				<div class="legal">
					<?php
					$defaults = array(
						'theme_location'  => 'footer-legal',
						'container'       => 'div',
						'container_class' => 'legal footer-legal',
						'container_id'    => 'legal_section',
						'menu_class'      => 'menu',
						'menu_id'         => 'menu-legal',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);
					wp_nav_menu( $defaults );
					?>
				</div>
			</div>
		</div>
	</div>
</footer>