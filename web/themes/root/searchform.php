<div class="search-box pull-right no-right-pad">
	<form action="<?php echo home_url( '/' ); ?>" method="get" class="navbar-form navbar-left pull-right no-right-pad">
		<fieldset>
			<div class="input-group">
				<input type="text" name="s" id="search" placeholder="<?php _e("Search","LearnerLaneTheme"); ?>" value="<?php the_search_query(); ?>" class="form-control" />
				<span class="input-group-btn">
					<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</fieldset>
	</form>
</div>
				