<?php
/*
Template Name: Front Page
*/
?>
<?php get_header(); ?>

<div class="frontpage-container">
	<section id="content">
		<article id="frontpage" <?php post_class(); ?>>
			<?php if( have_posts() ): ?>
				<?php while( have_posts() ): ?>
					<?php the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</article>
	</section>
</div>

<?php get_footer(); ?>