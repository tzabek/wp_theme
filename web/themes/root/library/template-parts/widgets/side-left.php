<article id="widget-left" class="clearfix">
	<?php if( is_page_template('styleguide.php') ): ?>
		<?php learnerlane_styleguide_nav(); ?>
	<?php else: ?>
		<?php dynamic_sidebar('Left'); ?>
	<?php endif; ?>
</article>