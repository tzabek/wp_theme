<?php get_header(); ?>

<div class="container">
	<section id="content">
		<?php get_template_part( 'library/template-parts/widgets/side', 'left' ); ?>
		<article id="main-body">
			<?php get_template_part( 'library/template-parts/widgets/side', 'top' ); ?>
			<?php get_template_part( 'library/template-parts/loops/loop', 'page' ); ?>
			<?php get_template_part( 'library/template-parts/widgets/side', 'bottom' ); ?>
		</article>
		<?php get_template_part( 'library/template-parts/widgets/side', 'right' ); ?>
	</section>
</div>

<?php get_footer(); ?>