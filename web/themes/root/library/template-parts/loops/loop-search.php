<div class="">
	<h1 class="page-title"><span><?php _e("Search Results for","Root"); ?>:</span> <?php echo esc_attr(get_search_query()); ?></h1>
</div>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article id="post-&lt;?php the_ID(); ?&gt;" <?php post_class('clearfix'); ?> role="article">
		<header>
			<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
			<p class="meta">
				<?php _e("Posted", "Root"); ?> <time datetime="<?php echo the_time('Y-m-j'); ?>" pubdate>
				<?php the_date(); ?></time>
			</p>
		</header>
		<section class="post_content">
			<?php the_excerpt('<span class="read-more">' . __("Read more on","Root") . ' "'.the_title('', '', false).'" &raquo;</span>'); ?>
		</section>
		<footer>
			
		</footer>
	</article>

	<?php endwhile; ?>
		<?php if (function_exists("emm_paginate")) : ?>
			<?php emm_paginate(); ?>
		<?php else : ?>
	
			<nav class="wp-prev-next">
				<ul class="clearfix">
					<li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "Root")) ?></li>
					<li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "Root")) ?></li>
				</ul>
			</nav>
			
		<?php endif; ?>
	<?php else : ?>

	<article id="post-not-found">
		<header>
			<h1><?php _e("Not Found", "Root"); ?></h1>
		</header>
		<section class="post_content">
			<p><?php _e("Sorry, but the requested resource was not found on this site.", "Root"); ?></p>
		</section>
		<footer>
			
		</footer>
	</article>

<?php endif; ?>
