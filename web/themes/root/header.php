<?php 
global $root_options;
$root_settings = get_option('root_options', $root_options);
$brand_logo = $root_settings['brand_logo'];
$brand_logo_inside = ( $root_settings['brand_logo_inside'] == '' ) ? $brand_logo : $root_settings['brand_logo_inside'];

$the_logo = ( is_home() || is_front_page() ) ? $brand_logo : $brand_logo_inside;
?>

<!DOCTYPE html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
	<meta charset="utf-8">
	
	<?php // Google Chrome Frame for IE ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title><?php wp_title(''); ?></title>
	
	<?php // mobile meta ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	
	<?php if ( $root_settings['fav_icon'] != '' ) : ?><link rel="shortcut icon" href="<?php echo $root_settings['fav_icon']; ?>"><?php endif; ?>
	<?php if ( $root_settings['touch_icon'] != '' ) : ?><link rel="apple-touch-icon" href="<?php echo $root_settings['touch_icon']; ?>"><?php endif; ?>
	<!--[if IE]>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
	<![endif]-->
	
	<meta name="msapplication-TileColor" content="#f01d4f">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	<?php wp_head(); ?>

	<!--[if lt IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script><![endif]-->
</head>

<body <?php body_class(); ?>>
<header id="header" class="container-fluid">
	<div class="navbar-container">
		<nav class="navbar navbar-default" role="navigation">
		
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#globalnav-collapse">
					<span class="sr-only">Toggle navigation</span>
					<i class="fa fa-list"></i>
				</button>
				<a href="<?php bloginfo( 'url' ) ?>/" title="<?php bloginfo( 'name' ) ?>" rel="homepage" class="navbar-brand" id="branding">
					<?php if( $the_logo ): ?>
						<img src="<?php echo $the_logo; ?>" alt="<?php wp_title(); ?>" title="<?php wp_title(); ?>">
					<?php else: ?>
						<?php bloginfo('title'); ?>
					<?php endif; ?>
				</a>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="globalnav-collapse">
				<?php if( is_user_logged_in() ): ?>
					<?php root_logged_nav(); ?>
				<?php else: ?>
					<?php root_main_nav(); ?>
				<?php endif; ?>
			</div>
		</nav>
	</div>
</header>
<?php /*
<!-- Inside logo if needed
<?php if ( $brand_logo_inside ) : ?><img src="<?php echo $brand_logo_inside; ?>" alt="<?php wp_title(); ?>" title="<?php wp_title(); ?>">
<?php else : bloginfo('title'); ?>
<?php endif; ?>
-->
*/ ?>