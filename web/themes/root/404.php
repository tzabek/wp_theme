<?php get_header(); ?>

<div class="container">
	<section id="content">
		<?php get_template_part( 'library/template-parts/widgets/side', 'left' ); ?>
		<article id="front-page">
				<h1>404</h1>
				<p>Sorry, the page you were looking for was not found.</p>
		</article>
		<?php get_template_part( 'library/template-parts/widgets/side', 'right' ); ?>
	</section>
</div>

<?php get_footer(); ?>
