<?php 
/*
Template Name: Contact
*/
get_header();
?>

<div id="contact" class="page-container">
	<div class="container">
		<section id="content" class="clearfix">
			<article id="main-body" class="">
				<?php the_content(); ?>
			</article>
			<aside id="sidebar">
				<div class="sidebar sidebar-inner"></div>
			</aside>
		</section>
	</div>
	<?php get_template_part( 'entry', 'footer' ); ?>
</div>

<?php get_footer(); ?>