<?php

add_action('wp_install', 'aka_wp_install');
function aka_wp_install($user)
{
    // Set default theme
    $dtheme = wp_get_theme('generic');
    if($dtheme->exists() && $dtheme->is_allowed()) {
        switch_theme($dtheme->get_stylesheet());
    }

    // Set blog description (tagline) to blank
    update_option('blogdescription', '');
}